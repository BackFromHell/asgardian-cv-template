% | Asgardian's Resume/CV Template.
% | LaTeX Template
% | Version 0.0.1 (2019/05/21)
% |
% | Original author:
% | Thierry Leurent <thierry.leurent@asgardian.be>
% |
% | License:
% | GNU GPLV3

\ProvidesClass{friggeri-cv}[2012/04/30 CV class]
\NeedsTeXFormat{LaTeX2e}

\DeclareOption{print}{\def\@cv@print{}}
\DeclareOption*{%
  \PassOptionsToClass{\CurrentOption}{article}%
}

\ProcessOptions\relax
\LoadClass{article}

\usepackage[utf8]{inputenc}
\usepackage[a4paper]{geometry}
\geometry{top=1cm, bottom=1.5cm, left=6cm, right=1cm}

% | Lorem Ipsum : https://www.lipsum.com/
\usepackage{lipsum}

% | Align
\usepackage[absolute,overlay]{textpos}
\usepackage{ragged2e}
\usepackage{multicol}
\usepackage{fancyhdr}
\usepackage{lastpage}

% | Color
\usepackage[dvipsnames]{xcolor}
\definecolor{crimson}{RGB}{220, 20, 60}

% | Fonts
\usepackage{./fontawesome5}

% | QR Code
\usepackage[final]{qrcode}

% | HRef, URL, .......
\usepackage{hyperref}
% I want not a different colot for URI.
\hypersetup{
    colorlinks=true,
    linkcolor=black,
    filecolor=black,
    urlcolor=black,
}

\urlstyle{same}

% | If and friends.
\usepackage{xifthen}% provides \isempty test
\usepackage{ifthen}% provides \equal test
\usepackage{xstring}

% | ??????
\usepackage{scrextend}
\usepackage{setspace}

% | Style Customization.
% ----------------------------------------------------------------
% | From Section to Paragraph.
\usepackage[compact]{titlesec}
\titleformat{\section}{\color{crimson}\bfseries\normalsize\raggedright}{}{0em}{}[\color{crimson}\titlerule]
\titlespacing{\section}{0pt}{0pt}{0.2cm}

\titleformat{\subsection}{
  \normalsize\raggedright
}{}{0em}{}[]
\titlespacing{\subsection}{0cm}{0cm}{0cm}

\titleformat{\subsubsection}{
  \normalsize\raggedright
}{}{1em}{}[]
\titlespacing{\subsubsection}{0cm}{0cm}{0cm}

\setlength{\parindent}{0pt} % Stop paragraph indentation
\titleformat{\paragraph}[runin]{
  \bfseries\normalsize\raggedright
}{}{1em}{\hspace{1em}}{\smallskip}

% | Header and Footer
\pagestyle{fancy}
\fancyhead{}
\renewcommand{\headrulewidth}{0pt}
\fancyfoot{}
\fancyfoot[R]{\thepage\textcolor{crimson}{/}\pageref{LastPage}}

% | Lists : Custom items.
\renewcommand{\labelitemi}{\textcolor{crimson}{$\circ$}}
\renewcommand{\labelitemii}{\textcolor{crimson}{$-$}}
\renewcommand{\labelitemiii}{\textcolor{crimson}{$\diamond$}}

% | Customized Itemize.
\newenvironment{project_task}{
  \begin{itemize}
    \setlength{\itemsep}{0.05cm}
    \setlength{\parskip}{0cm}
    \setlength{\parsep}{0cm}
  }{\end{itemize}}

\newenvironment{cvTechnologies}{
  \begin{itemize}
    \setlength{\itemsep}{0.01cm}
    \setlength{\parskip}{0cm}
    \setlength{\parsep}{0cm}
  }{\end{itemize}}

% | Custom Commands.
% ----------------------------------------------------------------
% | Add C.V. Title.
\newcommand{\CVTitle}[3]{
  \parbox{13.5cm}{\fontsize{20}{30}\selectfont\centering \textcolor{gray}{#1} \textcolor{darkgray}{#2}\\
  \fontsize{12}{14}\selectfont\textcolor{crimson}{#3}}
  \\\\\\
}
% | Add the column on left.
\newcommand{\LeftBlock}[1]{
  \addcontentsline{toc}{section}{Contact}
  \addcontentsline{toc}{section}{Web}
  \addcontentsline{toc}{section}{Top Skills}
  \begin{textblock*}{5cm}(.5cm,1cm)
    \LeftBlockSection{Contact}
    {\icontext{\hspace{.1cm}\textbf{@}}{\href{mailto:\email}{\email}}
      \icontext{\faicon{map-marker-alt}}{\location}
      \icontext{\faicon{phone}}{\phonenumber}}
    #1
  \end{textblock*}

  \begin{textblock*}{5cm}(0.5cm,22cm)
    % The QRCode.
    \begin{minipage}[t]{30mm}
      \vspace{5mm}
      \qrcode[version=3]{
        BEGIN:VCARD\?
        VERSION:4.0\?
        FN:\firstname \lastname\?
        N:\lastname;\firstname\?
        GENDER:\gender\?
        KIND:individual\?
        TITLE:\slogan\?
        ADR;TYPE=HOME;;\locationStreet;;\locationCity;\locationZip;\locationCountry\?
        TEL;TYPE=mobile,home:tel:\phonenumber\?
        EMAIL;TYPE=internet,home:\email\?
        % URL:http://www.bortzmeyer.org/
        BDAY:\bdayYear\bdayMounth\bdayDay\?
        TZ:Brussels/Europe\?
        LANG;PREF=1:fr\?
        LANG;PREF=2:en\?
        END:VCARD
        }
      \vspace{5mm}
    \end{minipage}
  \end{textblock*}

  \begin{textblock*}{5cm}(.5cm,27cm)
    \begin{spacing}{0.25}
      \tiny
      Create using :
      \vspace{-0.5em}
      \begin{cvTechnologies}
      \item \href{https://www.latex-project.org/}{\LaTeX}.
      \item \href{https://en.wikipedia.org/wiki/Emacs}{Emacs 26.1}.
      \item \href{https://en.wikipedia.org/wiki/PdfTeX}{pdfTeX}.
      \item \href{https://gitlab.com/BackFromHell/asgardian-cv-template}{Asgardian's C.V. Template by T. Leurent}.
      \item \href{https://gitlab.com/BackFromHell/asgardian-font-awesome-binding-for-latex}{Asgardian's font awsome binding for \LaTeX}.
      \end{cvTechnologies}
    \end{spacing}
    \normalsize
  \end{textblock*}

  \begin{textblock*}{5cm}(.5cm,29.2cm)
    \textcolor{gray}{Last Updated on } \textcolor{darkgray}{\today}
    \end{textblock*}
}
% | Add a section in the column on left.
\newcommand{\LeftBlockSection}[2]{
  \section*{#1}
  #2
  \medskip\medskip
}

% |Command to display icons
\newcommand{\icon}[1]{\colorbox{crimson}{\makebox(9,9){\hspace{-.1cm}\textcolor{white}{#1}}}}
\newcommand{\icontext}[2]{\footnotesize\makebox[.75cm]{\icon{#1}} \parbox[t]{5cm}{#2}\par}

% | Add a CV Entry.
% | Arguments :
% |   - Company / Client.
% |   -  Period.
% |   -  Introduction.
% |   -  Role.
% |   -  Description.
% |   - Key Technologies.
% ----------------------------------------------------------------
% | @ Company Level
\newcommand{\cvEntryCompany}[6]{
  \cvEntry{Company}{#1}{#2}{#3}{#4}{#5}{#6}
}
% | @ Client Level
\newcommand{\cvEntryClient}[6]{
  \cvEntry{Client}{#1}{#2}{#3}{#4}{#5}{#6}
}

\newcommand{\cvEntry}[7]{
  \cvEntryHeader{#1}{#2}{#3}{#4}{#5}
  \ifx&#6&%
       {~~\\}
    \else
       {#6}
    \fi
  \ifthenelse{\isempty{#7}}{}
  {\cvEntryKeyTechnologies{#1}{#7}}
}
\newcommand{\cvEntryHeader}[5]{
  \IfStrEq{#1}{Company}
  {\subsection{#2}}
  {\subsubsection{#2}}
  \vspace{-0.1cm}\vspace{-1em}\begin{flushright}{#3}\end{flushright}
  \normalsize
  \ifthenelse{\isempty{#4}}{}
  {\vspace{-0.75em}\emph{#4}}
  \ifthenelse{\isempty{#5}}{}
  {\IfStrEq{#1}{Company}
    {\vspace{-1em}\textcolor{crimson}{#5}}
    {\vspace{-1em}\hspace{1em}\textcolor{crimson}{#5}}
  }
}
% | Project Entry : Add a project entry and use LaTeX syntax.
% ----------------------------------------------------------------
\newcommand{\cvEntryProject}[3]{
  \paragraph{#1}
  % ~~\\
  \begin{addmargin}[1em]{1em}
    \vspace{0.25em}
    \emph{#2}
    #3
  \end{addmargin}
}
\newcommand{\cvEntryKeyTechnologies}[2]{
  \setlength{\columnseprule}{0.01mm}
  \def\columnseprulecolor{\color{crimson}}
  \IfStrEq{#1}{Company}
  {\textcolor{crimson}{K}ey \textcolor{crimson}{T}echnologies.
    \begin{multicols}{3}
      #2
    \end{multicols}}
  {\begin{addmargin}[1em]{1em}
    \textcolor{crimson}{K}ey \textcolor{crimson}{T}echnologies.
    \begin{multicols}{3}
      #2
    \end{multicols}
  \end{addmargin}}

}

\newcommand{\EducationEntry}[4]{
  \subsection{#1}
  \vspace{-0.1cm}\vspace{-1em}\begin{flushright}{#2}\end{flushright}
  \vspace{-1em}\textcolor{crimson}{#3 : #4}
}

\newcommand{\CourseEntry}[2]{
  \subsection{#1}
  \vspace{-0.1cm}\vspace{-1em}\begin{flushright}{#2}\end{flushright}
}

\newcommand{\CertificationEntry}[2]{
  \subsection{#1}
  \vspace{-0.1cm}\vspace{-1em}\begin{flushright}{#2}\end{flushright}
}

\newcommand{\LanguageEntry}[2]{
  \subsection{#1}
  \vspace{-0.1cm}\vspace{-1em}\begin{flushright}{#2}\end{flushright}
}
\newcommand{\getParLevel}{%
  \ifnum\value{section}=0
    chapter%
  \else
    \ifnum\value{subsection}=0
      section%
      \else
      \ifnum\value{subsubsection}=0
      subsection%
      \else
      subsubsection%
      \fi
    \fi
  \fi
}
