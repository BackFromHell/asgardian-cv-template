# Asgardian CV Template

A simple but cute CV template.
This is a two column template, the one on the  left contain contact information, web information, top skills and a QR-Code. The one on the right contains the CV itself.
Some icons from the font awesome give a nice look.